/*
   Simple IR Sensor to dump data over the serial port
   Optogenetics and Neural Engineering Core ONE Core
   University of Colorado
   5/7/2019
   See https://optogeneticsandneuralengineeringcore.gitlab.io/ONECoreSite/ for more information, including a detailed write up
   
*/


#include <Wire.h>
#include <Adafruit_MLX90614.h>

Adafruit_MLX90614 mlx = Adafruit_MLX90614();

void setup() {
  Serial.begin(9600);

  Serial.println("ONECore Rules!");

  mlx.begin();
}

void loop() 
{
  // To slow the data and average over several reading, increase the number to average over (num2avgover):
  int num2avgover = 1;  //Fast, lots of data
  float averagesum = 0;  
  
  for (int i=0; i < num2avgover; i++) 
  {
    float temp = mlx.readObjectTempC();
    averagesum = averagesum + temp;
  }
  float average = averagesum/num2avgover;
  
  Serial.print(average);
  Serial.println(" *C");
}
